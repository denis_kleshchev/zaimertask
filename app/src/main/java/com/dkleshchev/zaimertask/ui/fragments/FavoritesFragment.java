package com.dkleshchev.zaimertask.ui.fragments;

import com.dkleshchev.zaimertask.R;
import com.dkleshchev.zaimertask.model.Card;

import java.util.Iterator;

public class FavoritesFragment extends PageBaseFragment {

    @Override
    protected String getTitle() {
        return getString(R.string.favorites_title);
    }

    @Override
    public AppearanceType getFragmentAppearance() {
        return AppearanceType.FAVORITES;
    }

    @Override
    protected void onPostResume() {
        subscribe(bookManager.getFavoriteBooks(),
                this::setUI,
                error -> {
                    setScreenState(TabRootFragment.ScreenState.ERROR);
                    logger.warn("error occurred: ", error);
                });

    }

    @Override
    protected void onFavoriteChanged(Card card, boolean inFavoritesNow) {
        final Iterator<Card> iterator = cards.iterator();
        while (iterator.hasNext()) {
            final Card next = iterator.next();
            if (next.equals(card)) {
                iterator.remove();
                break;
            }
        }
        booksAdapter.notifyDataSetChanged();
    }
}
