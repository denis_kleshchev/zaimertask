package com.dkleshchev.zaimertask.ui;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DividerDecorator extends RecyclerView.ItemDecoration {
    private final int offset;

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = offset;
        }
        outRect.bottom = offset;
    }
}
