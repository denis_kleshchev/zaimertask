package com.dkleshchev.zaimertask.ui.fragments;

import com.dkleshchev.zaimertask.R;
import com.dkleshchev.zaimertask.model.Card;
import com.dkleshchev.zaimertask.utils.RxUtils;

public class BooksSearchFragment extends PageBaseFragment {

    @Override
    protected String getTitle() {
        return getString(R.string.search_title);
    }

    @Override
    public AppearanceType getFragmentAppearance() {
        return AppearanceType.SEARCH;
    }

    @Override
    protected void onPostResume() {
        if (getMainActivity() != null) {
            subscribe(getMainActivity()
                            .searchObservable()
                            .flatMap(RxUtils.transformAsync(s -> {
                                setScreenState(TabRootFragment.ScreenState.PROGRESS);
                                return s;
                            }))
                            .flatMap(s -> bookManager.searchBooks(s)),
                    this::setUI,
                    error -> {
                        setScreenState(TabRootFragment.ScreenState.ERROR);
                        logger.warn("error occurred: ", error);
                    });
        }
    }

    @Override
    protected void onFavoriteChanged(Card card, boolean inFavoritesNow) {
        if (cards.contains(card)) {
            final Card fromList = cards.get(cards.indexOf(card));
            fromList.setInFavorites(inFavoritesNow);
            booksAdapter.notifyDataSetChanged();
        }
    }
}
