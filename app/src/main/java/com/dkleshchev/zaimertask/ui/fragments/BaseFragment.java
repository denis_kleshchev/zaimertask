package com.dkleshchev.zaimertask.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.dkleshchev.zaimertask.di.Injector;
import com.dkleshchev.zaimertask.manager.SubscriptionManager;
import com.dkleshchev.zaimertask.ui.activity.MainActivity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

public abstract class BaseFragment extends Fragment {
    protected final Logger logger = LoggerFactory.getLogger(getClass().getSimpleName());
    protected final SubscriptionManager subscriptionManager = new SubscriptionManager();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logger.debug("onCreate()");
        Injector.getComponent().inject(this);
        injectDependencies();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        logger.debug("onViewCreated()");
        ButterKnife.bind(this, view);
        onPostViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        logger.debug("onDestroyView()");
        Glide.get(getContext()).clearMemory();
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        logger.debug("onResume()");
        setUpAppearance();
    }

    @Override
    public void onPause() {
        super.onPause();
        logger.debug("onPause()");
        subscriptionManager.closeManagedSubscriptions();
    }

    protected void onPostViewCreated(View view, final Bundle savedInstanceState) {
    }

    protected abstract int getLayoutId();

    protected abstract void injectDependencies();

    protected String getTitle() {
        return "";
    }

    public AppearanceType getFragmentAppearance() {
        return AppearanceType.NO_INFLUENCE;
    }

    protected void setUpAppearance() {
        setUpAppearance(getFragmentAppearance(), getTitle());
    }

    protected void setUpAppearance(final AppearanceType type,
                                   final String title) {
        if (getMainActivity() != null) {
            getMainActivity().setUpAppearance(type, title);
        }
    }

    protected MainActivity getMainActivity() {
        final FragmentActivity activity = getActivity();
        return activity instanceof MainActivity ? (MainActivity) activity : null;
    }

    protected TabRootFragment getParent() {
        if (getParentFragment() instanceof TabRootFragment) {
            return (TabRootFragment) getParentFragment();
        }

        return null;
    }

    protected void setScreenState(final TabRootFragment.ScreenState state) {
        if (getParent() != null) {
            getParent().setScreenState(state);
        }
    }

    public <T> Subscription subscribe(final Observable<T> observable, final Action1<T> next, final Action1<Throwable> error) {
        return subscriptionManager.subscribe(observable, next, error);
    }

    public enum AppearanceType {
        SEARCH,
        FAVORITES,
        NO_INFLUENCE
    }
}
