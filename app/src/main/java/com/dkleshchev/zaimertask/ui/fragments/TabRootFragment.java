package com.dkleshchev.zaimertask.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dkleshchev.zaimertask.R;
import com.dkleshchev.zaimertask.utils.ViewUtils;

import butterknife.BindView;

public class TabRootFragment extends BaseFragment {
    private static final String SEARCH_TAB = "search_tab";
    private static final String FAVORITES_TAB = "favorites_tab";

    @BindView(R.id.above_content_frame)
    View mask;
    @BindView(R.id.progress_bar)
    View progressBar;
    @BindView(R.id.message_text)
    TextView maskMessage;

    private FragmentTabHost tabHost;

    public static TabRootFragment newInstance() {

        return new TabRootFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_root_tabhost;
    }

    @Override
    protected void injectDependencies() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        tabHost = new FragmentTabHost(getActivity());
        inflater.inflate(R.layout.fragment_root_tabhost, tabHost);

        tabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);

        initializeTabs();

        return tabHost;
    }

    private void initializeTabs() {
        final View tabSearch = ViewUtils.getTabView(getContext(), R.drawable.search_selector);
        final View tabFavorites = ViewUtils.getTabView(getContext(), R.drawable.favorites_selector);

        tabHost.addTab(
                tabHost.newTabSpec(SEARCH_TAB).setIndicator(tabSearch),
                BooksSearchFragment.class,
                null
        );

        tabHost.addTab(
                tabHost.newTabSpec(FAVORITES_TAB).setIndicator(tabFavorites),
                FavoritesFragment.class,
                null
        );
    }

    public void setScreenState (final ScreenState state) {
        switch (state){
            case CONTENT:
                ViewUtils.changeViewVisibility(false, mask);
                break;
            case PROGRESS:
                ViewUtils.changeViewVisibility(true, mask, progressBar);
                ViewUtils.changeViewVisibility(false, maskMessage);
                break;
            case NO_CONTENT:
                ViewUtils.changeViewVisibility(false, progressBar);
                ViewUtils.changeViewVisibility(true, mask, maskMessage);
                maskMessage.setText(getString(R.string.no_content_message));
                break;
            case ERROR:
                ViewUtils.changeViewVisibility(false, progressBar);
                ViewUtils.changeViewVisibility(true, mask, maskMessage);
                maskMessage.setText(getString(R.string.error_message));
                break;
        }
    }

    public enum ScreenState {
        CONTENT,
        PROGRESS,
        NO_CONTENT,
        ERROR
    }
}
