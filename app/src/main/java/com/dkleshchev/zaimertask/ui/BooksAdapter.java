package com.dkleshchev.zaimertask.ui;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dkleshchev.zaimertask.R;
import com.dkleshchev.zaimertask.model.Card;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.AllArgsConstructor;
import lombok.Setter;
import rx.functions.Action1;

@AllArgsConstructor
public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {
    @Setter
    private List<Card> items;
    private final Context context;
    @Setter
    private Action1<Card> favoriteListener = null;
    @Setter
    private Action1<String> downloadListener = null;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Card item = getItem(position);
        if (item != null) {
            holder.title.setText(item.getTitle());
            holder.author.setText(item.getAuthor());
            if (!TextUtils.isEmpty(item.getCoverLink())) {
                Glide
                        .with(context)
                        .load(item.getCoverLink())
                        .centerCrop()
                        .crossFade()
                        .into(holder.cover);
            } else {
                holder.cover.setImageResource(R.drawable.no_cover);
            }

            holder.favorite.setOnClickListener(view -> {
                if (favoriteListener != null) {
                    favoriteListener.call(item);
                }
            });

            holder.download.setOnClickListener(view -> {
                if (downloadListener != null) {
                    downloadListener.call(item.getDownloadLink());
                }
            });

            final @DrawableRes int favoriteIcon = item.isInFavorites() ? R.drawable.in_favorites : R.drawable.not_in_favorites;

            holder.favorite.setImageResource(favoriteIcon);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private Card getItem(final int position) {
        if (items.size() > position) {
            return items.get(position);
        }
        return null;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.book_cover)
        ImageView cover;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.author)
        TextView author;
        @BindView(R.id.download)
        ImageButton download;
        @BindView(R.id.favorite)
        ImageButton favorite;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
