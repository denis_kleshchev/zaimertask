package com.dkleshchev.zaimertask.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dkleshchev.zaimertask.R;
import com.dkleshchev.zaimertask.data.network.api.Api;
import com.dkleshchev.zaimertask.di.Injector;
import com.dkleshchev.zaimertask.ui.fragments.BaseFragment;
import com.dkleshchev.zaimertask.ui.fragments.TabRootFragment;
import com.dkleshchev.zaimertask.utils.FragmentUtils;
import com.dkleshchev.zaimertask.utils.RxUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

@SuppressWarnings("CreateIntent")
public class MainActivity extends AppCompatActivity {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainActivity.class.getSimpleName());

    @BindView(R.id.container)
    protected ViewGroup container;
    @BindView(R.id.toolbar)
    protected Toolbar toolbarView;
    @BindView(R.id.screen_title)
    protected TextView screenTitle;

    @Inject
    Api api;

    private Menu menu;
    private SearchView searchView;

    private String searchQuery;

    private FragmentManager fragmentManager;

    private PublishSubject<String> searchSubject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        fragmentManager = getSupportFragmentManager();
        Injector.getComponent().inject(this);
        setSupportActionBar(toolbarView);
        searchSubject = PublishSubject.create();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;

        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.menu_search));
        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    if (searchSubject != null) {
                        searchSubject.onNext(searchQuery);
                        searchView.clearFocus();
                    }
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    searchQuery = newText;
                    return true;
                }
            });

            searchView.setOnQueryTextFocusChangeListener((view, hasFocus) -> {
                        if (!hasFocus) {
                            searchView.setIconified(true);
                        }
                    }
            );
        }


        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fragmentManager.getBackStackEntryCount() == 0) {
            FragmentUtils.startFragment(fragmentManager, TabRootFragment.newInstance(), container.getId(), TabRootFragment.class.getSimpleName());
        }
    }

    @Override
    public void onBackPressed() {
        if (searchView.hasFocus()) {
            collapseSearchView();
            return;
        }

        finish();
    }

    public void setUpAppearance(final BaseFragment.AppearanceType type,
                                final String title) {
        switch (type) {
            case SEARCH:
                setSearchVisibility(true);
                break;
            case FAVORITES:
                setSearchVisibility(false);
                break;
            case NO_INFLUENCE:
                break;
        }

        screenTitle.setText(title);
    }

    public Observable<String> searchObservable() {
        return searchSubject
                .asObservable()
                .compose(RxUtils.applySchedulers());
    }

    private void collapseSearchView() {
        searchView.clearFocus();
        searchView.setIconified(true);
    }

    private void setSearchVisibility(final boolean visible) {
        if (menu != null) {
            menu.findItem(R.id.menu_search).setVisible(visible);
        }
    }
}
