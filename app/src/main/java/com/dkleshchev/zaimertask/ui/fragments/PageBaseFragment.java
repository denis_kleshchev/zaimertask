package com.dkleshchev.zaimertask.ui.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.dkleshchev.zaimertask.R;
import com.dkleshchev.zaimertask.di.Injector;
import com.dkleshchev.zaimertask.manager.BookManager;
import com.dkleshchev.zaimertask.model.Card;
import com.dkleshchev.zaimertask.ui.BooksAdapter;
import com.dkleshchev.zaimertask.ui.DividerDecorator;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import rx.functions.Action1;

public class PageBaseFragment extends BaseFragment {
    private static final String CARDS_KEY = "saved_cards";

    @BindView(R.id.recycler)
    protected RecyclerView recycler;

    @Inject
    protected BookManager bookManager;

    protected BooksAdapter booksAdapter = null;
    protected final ArrayList<Card> cards = new ArrayList<>();
    protected final ArrayList<Card> savedCards = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.recycler_fragment;
    }

    @Override
    protected void injectDependencies() {
        Injector.getComponent().inject(this);
    }

    @Override
    protected void onPostViewCreated(View view, Bundle savedInstanceState) {
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.addItemDecoration(new DividerDecorator((int) getResources().getDimension(R.dimen.activity_horizontal_margin)));

        if (savedInstanceState != null) {
            final ArrayList<Card> savedInstanceStateCards = savedInstanceState.getParcelableArrayList(CARDS_KEY);
            if (savedInstanceStateCards != null) {
                this.savedCards.addAll(savedInstanceStateCards);
            }
        }

        booksAdapter = new BooksAdapter(this.cards, getContext(), onFavoriteClick(), onDownloadClick());
        recycler.setAdapter(booksAdapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        logger.debug("onSaveInstanceState()");
        if (outState != null) {
            outState.putParcelableArrayList(CARDS_KEY, cards);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUI(savedCards);
        onPostResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (savedCards.isEmpty()) {
            savedCards.addAll(cards);
        }
    }

    protected void onPostResume() {

    }

    protected void setUI(final List<Card> cards) {
        if (cards == null || cards.isEmpty()) {
            setScreenState(TabRootFragment.ScreenState.NO_CONTENT);
            return;
        }

        logger.debug("Got {} cards", cards.size());
        this.cards.clear();
        this.cards.addAll(cards);
        savedCards.clear();
        booksAdapter.notifyDataSetChanged();
        setScreenState(TabRootFragment.ScreenState.CONTENT);
    }

    private Action1<String> onDownloadClick() {
        return s -> Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

    private Action1<Card> onFavoriteClick() {
        return card -> subscribe(
                bookManager.swapFavorite(card),
                aBoolean -> onFavoriteChanged(card, aBoolean),
                error -> {
                    //do nothing
                }
        );
    }

    protected void onFavoriteChanged(final Card card,
                                     final boolean inFavoritesNow) {

    }
}
