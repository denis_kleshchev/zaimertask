package com.dkleshchev.zaimertask.di;

import com.dkleshchev.zaimertask.ui.activity.MainActivity;
import com.dkleshchev.zaimertask.ui.fragments.BaseFragment;
import com.dkleshchev.zaimertask.ui.fragments.BooksSearchFragment;
import com.dkleshchev.zaimertask.ui.fragments.FavoritesFragment;
import com.dkleshchev.zaimertask.ui.fragments.PageBaseFragment;

import javax.inject.Singleton;

@Singleton
@dagger.Component(modules = {AppModule.class})
public interface Component {
    void inject(BaseFragment fragment);
    void inject(PageBaseFragment fragment);
    void inject(BooksSearchFragment fragment);
    void inject(FavoritesFragment fragment);
    void inject(MainActivity mainActivity);
}
