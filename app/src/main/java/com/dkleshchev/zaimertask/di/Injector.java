package com.dkleshchev.zaimertask.di;

import lombok.Getter;
import lombok.Setter;

public class Injector {
    @Getter
    @Setter
    private static Component component;
}
