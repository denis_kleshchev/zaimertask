package com.dkleshchev.zaimertask.di;

import android.content.Context;

import com.dkleshchev.zaimertask.ZaimerApplication;
import com.dkleshchev.zaimertask.data.database.DatabaseHelper;
import com.dkleshchev.zaimertask.data.network.api.Api;
import com.dkleshchev.zaimertask.manager.BookManager;
import com.dkleshchev.zaimertask.utils.NetworkUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lombok.AllArgsConstructor;
import okhttp3.OkHttpClient;

@Module
@AllArgsConstructor
public class AppModule {
    private final ZaimerApplication application;

    @Provides
    @Singleton
    ZaimerApplication providesZaimerApplication() {
        return application;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttp() {
        return NetworkUtils.getClient();
    }

    @Provides
    @Singleton
    Api providesApi(final Context context, final OkHttpClient client) {
        return new Api(context, client);
    }

    @Provides
    @Singleton
    BookManager providesBookManager(final Context context, final Api api, final DatabaseHelper databaseHelper) {
        return new BookManager(context, api, databaseHelper);
    }

    @Provides
    @Singleton
    DatabaseHelper providesDatabaseHelper(final Context context) {
        return new DatabaseHelper(context);
    }
}
