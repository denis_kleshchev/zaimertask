package com.dkleshchev.zaimertask.utils;

import android.os.AsyncTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public final class RxUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(RxUtils.class.getSimpleName());
    private static final Scheduler ASYNC_SCHEDULER = Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR);
    public static final Scheduler MAIN_SCHEDULER = AndroidSchedulers.mainThread();

    private RxUtils() {
    }

    public static <T> Observable.Transformer<T, T> applySchedulers(final Scheduler subscribeOn,
                                                                   final Scheduler observeOn) {
        return observable -> observable.subscribeOn(subscribeOn).observeOn(observeOn);
    }

    public static <T> Observable.Transformer<T, T> applySchedulers() {
        return applySchedulers(ASYNC_SCHEDULER, MAIN_SCHEDULER);
    }

    public static <T> Observable<T> createFromFunction(final DataGenerator<T> generator) {
        return Observable.create(subscriber -> {
            subscriber.onStart();
            try {
                LOGGER.warn("createFromFunction() - started");
                subscriber.onNext(generator.generate());
                subscriber.onCompleted();
                LOGGER.warn("createFromFunction() - finished");
            } catch (Exception e) {
                LOGGER.warn("createFromFunction() - catch error({})", e.getClass().getSimpleName());
                subscriber.onError(e);
            }
        });
    }

    public static <T> Observable<T> runAsync(final DataGenerator<T> generator) {
        return createFromFunction(generator)
                .subscribeOn(ASYNC_SCHEDULER)
                .observeOn(ASYNC_SCHEDULER);
    }

    public static <IN, OUT> Func1<IN, Observable<OUT>> transformAsync(final DataTransformer<IN, OUT> transformer) {
        return in -> {
            try {
                return Observable
                        .just(transformer.transform(in))
                        .subscribeOn(ASYNC_SCHEDULER)
                        .observeOn(ASYNC_SCHEDULER);
            } catch (Exception e) {
                return Observable.error(e);
            }
        };
    }

    public interface DataTransformer<IN, OUT> {
        OUT transform(final IN in) throws Exception;
    }

    public interface DataGenerator<T> {
        T generate() throws Exception;
    }

}
