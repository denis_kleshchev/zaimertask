package com.dkleshchev.zaimertask.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.dkleshchev.zaimertask.R;

import butterknife.ButterKnife;

public final class ViewUtils {

    private ViewUtils() {
    }

    public static View getTabView(final Context context, final @DrawableRes int drawableId) {
        @SuppressLint("InflateParams") final View tabView = LayoutInflater.from(context).inflate(R.layout.item_tab, null);
        final ImageView image = ButterKnife.findById(tabView, R.id.tab_image);

        image.setImageDrawable(ContextCompat.getDrawable(context, drawableId));

        return tabView;
    }

    public static void changeViewVisibility(final boolean visibility,
                                            final View... views) {
        changeViewVisibility(visibility ? View.VISIBLE : View.GONE, views);
    }

    public static void changeViewVisibility(final int visibility,
                                            final View... views) {
        if (views != null && views.length > 0) {
            for (final View view : views) {
                if (view != null) {
                    view.setVisibility(visibility);
                }
            }
        }
    }
}
