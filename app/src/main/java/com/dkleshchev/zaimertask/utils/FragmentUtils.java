package com.dkleshchev.zaimertask.utils;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class FragmentUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FragmentUtils.class.getSimpleName());

    private FragmentUtils() {
    }

    public static void startFragment(final FragmentManager fragmentManager,
                                     final Fragment fragment,
                                     @IdRes final int containerId,
                                     final String fragmentName) {

        if (fragmentManager != null) {
            LOGGER.info("addNewFragment({})", fragmentManager.getBackStackEntryCount());
            LOGGER.debug("Fragment transaction: \n" +
                            "Fragment: {} to container: {}\n" +
                            "needAddToBackStack: {}\n" +
                            "needClearBackStack: {}\n" +
                            "needToReplace: {}\n" +
                            "removingFragment: {}\n" +
                            "fragmentName: {}\n" +
                            "animation: {}", fragment, containerId, true, false, false,
                    null, fragmentName);

            final FragmentTransaction transaction = fragmentManager.beginTransaction();

            transaction.add(containerId, fragment, fragmentName);

            transaction.addToBackStack(fragmentName);

            transaction.commitAllowingStateLoss();
        }
    }
}
