package com.dkleshchev.zaimertask.utils;

import com.dkleshchev.zaimertask.data.network.errorhandler.RxErrorHandlingCallAdapterFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;

public final class NetworkUtils {
    private static final int NETWORK_COMMUNICATION_TIMEOUT_SECONDS = 20;
    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkUtils.class.getSimpleName());

    public static <T> T createRestAdapter(final Class<T> restClientClass, final OkHttpClient client, Converter.Factory converterFactory, final String endpoint) {
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .client(client)
                .build();

        return retrofit.create(restClientClass);
    }

    public static OkHttpClient getClient() {
        final OkHttpClient httpClient = new OkHttpClient();
        final OkHttpClient.Builder builder = httpClient.newBuilder();

        final Logger logger = LoggerFactory.getLogger(Retrofit.class.getSimpleName());
        final HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(logger::debug);
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(httpLoggingInterceptor);
        builder.connectTimeout(NETWORK_COMMUNICATION_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        builder.readTimeout(NETWORK_COMMUNICATION_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        builder.writeTimeout(NETWORK_COMMUNICATION_TIMEOUT_SECONDS, TimeUnit.SECONDS);

        return builder.build();
    }
}
