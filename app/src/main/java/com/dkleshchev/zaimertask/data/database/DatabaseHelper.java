package com.dkleshchev.zaimertask.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.dkleshchev.zaimertask.data.database.entity.BookEntity;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseHelper.class.getSimpleName());

    private static final String DATABASE_NAME = "ZaimerTask.db";
    private static final int DATABASE_VERSION = 1;

    private ZaimerTaskDao zaimerTaskDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            LOGGER.debug("onCreate");
            TableUtils.createTable(connectionSource, BookEntity.class);
        } catch (SQLException e) {
            LOGGER.warn("Can't createWithBody database {}", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            LOGGER.info("onUpgrade");
            TableUtils.dropTable(connectionSource, BookEntity.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            LOGGER.warn("Can't drop databases {}", e);
            throw new RuntimeException(e);
        }
    }

    public synchronized ZaimerTaskDao getZaimerTaskDao() {
        if (zaimerTaskDao == null) {
            zaimerTaskDao = new ZaimerTaskDao(getRuntimeExceptionDao(BookEntity.class));
        }
        return zaimerTaskDao;
    }

    @Override
    public void close() {
        super.close();
        zaimerTaskDao = null;
    }
}
