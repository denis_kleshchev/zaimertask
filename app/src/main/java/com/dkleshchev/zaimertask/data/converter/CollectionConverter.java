package com.dkleshchev.zaimertask.data.converter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import rx.functions.Func1;

public abstract class CollectionConverter<BusinessObject, DataTransferObject> extends DataConverter<BusinessObject, DataTransferObject> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CollectionConverter.class.getSimpleName());

    public List<DataTransferObject> fromBusiness(@Nullable final Collection<BusinessObject> businessList) {
        return Utils.convertCollectionToList(businessList, this::fromBusiness);
    }

    public List<BusinessObject> fromDto(@Nullable final Collection<DataTransferObject> dtoList) {
        return Utils.convertCollectionToList(dtoList, this::fromDto);
    }

    private static class Utils {
        private Utils() {
        }

        static <From, To> List<To> convertCollectionToList(@Nullable final Collection<From> sourceList,
                                                           @NonNull Func1<From, To> convertFunction) {
            final ArrayList<To> resultList;
            if (sourceList == null || sourceList.isEmpty()) {
                resultList = new ArrayList<>();
            } else {
                resultList = new ArrayList<>(sourceList.size());
                for (final From from : sourceList) {
                    resultList.add(convertFunction.call(from));
                }
            }
            return resultList;
        }
    }

}
