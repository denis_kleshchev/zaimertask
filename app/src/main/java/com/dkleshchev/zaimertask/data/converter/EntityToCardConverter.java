package com.dkleshchev.zaimertask.data.converter;

import android.support.annotation.NonNull;

import com.dkleshchev.zaimertask.data.database.entity.BookEntity;
import com.dkleshchev.zaimertask.model.Card;

public class EntityToCardConverter extends CollectionConverter<BookEntity, Card> {
    @Override
    protected Card fromBusinessNullSafe(@NonNull BookEntity business) {
        return new Card(business.getId(),
                business.getTitle(),
                business.getAuthor(),
                business.getCoverLink(),
                business.getDownloadLink(),
                true);
    }

    @Override
    protected BookEntity fromDtoNullSafe(@NonNull Card dto) {
        final BookEntity bookEntity = new BookEntity();
        bookEntity.setId(dto.getId());
        bookEntity.setTitle(dto.getTitle());
        bookEntity.setAuthor(dto.getAuthor());
        bookEntity.setCoverLink(dto.getCoverLink());
        bookEntity.setDownloadLink(dto.getDownloadLink());
        return bookEntity;
    }
}
