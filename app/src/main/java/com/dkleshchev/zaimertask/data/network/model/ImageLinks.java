package com.dkleshchev.zaimertask.data.network.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ImageLinks {
    private String smallThumbnail;
    private String thumbnail;
}
