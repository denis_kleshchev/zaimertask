package com.dkleshchev.zaimertask.data.database.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@DatabaseTable(tableName = "favorites")
public class BookEntity {
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String AUTHOR = "author";
    public static final String COVER_LINK = "cover_link";
    public static final String DOWNLOAD_LINK = "download_link";

    @DatabaseField(id = true, columnName = ID)
    private String id;
    @DatabaseField(columnName = TITLE)
    private String title;
    @DatabaseField(columnName = AUTHOR)
    private String author;
    @DatabaseField(columnName = COVER_LINK)
    private String coverLink;
    @DatabaseField(columnName = DOWNLOAD_LINK)
    private String downloadLink;
}
