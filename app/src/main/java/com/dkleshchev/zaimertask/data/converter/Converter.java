package com.dkleshchev.zaimertask.data.converter;

import android.support.annotation.Nullable;

public interface Converter<BusinessObject, DataTransferObject> {

    DataTransferObject fromBusiness(@Nullable final BusinessObject business);

    BusinessObject fromDto(@Nullable final DataTransferObject dto);
}
