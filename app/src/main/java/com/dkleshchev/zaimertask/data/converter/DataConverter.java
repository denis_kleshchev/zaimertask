package com.dkleshchev.zaimertask.data.converter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class DataConverter<BusinessObject, DataTransferObject> implements Converter<BusinessObject, DataTransferObject> {
    @Override
    public DataTransferObject fromBusiness(@Nullable final BusinessObject business) {
        if (business != null) {
            return fromBusinessNullSafe(business);
        }
        return null;
    }

    @Override
    public BusinessObject fromDto(@Nullable final DataTransferObject dto) {
        if (dto != null) {
            return fromDtoNullSafe(dto);
        }
        return null;
    }

    protected abstract DataTransferObject fromBusinessNullSafe(@NonNull final BusinessObject business);

    protected abstract BusinessObject fromDtoNullSafe(@NonNull final DataTransferObject dto);
}
