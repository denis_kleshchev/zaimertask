package com.dkleshchev.zaimertask.data.network.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Kind {
    private String kind;
    private List<BookItem> items;
}
