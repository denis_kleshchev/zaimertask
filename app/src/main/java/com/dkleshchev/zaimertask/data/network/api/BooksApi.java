package com.dkleshchev.zaimertask.data.network.api;

import com.dkleshchev.zaimertask.data.network.model.Kind;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

@SuppressWarnings("WeakerAccess")
public interface BooksApi {
    @GET("books/v1/volumes")
    Observable<Kind> getBooksList(
            @Query("q") String query,
            @Query("key") String apiKey
    );
}
