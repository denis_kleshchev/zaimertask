package com.dkleshchev.zaimertask.data.database;

import com.dkleshchev.zaimertask.data.database.entity.BookEntity;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ZaimerTaskDao {
    private final RuntimeExceptionDao<BookEntity, String> bookDao;

    public boolean isInFavorites(final String id) {
        final BookEntity bookEntity = bookDao.queryForId(id);
        return bookEntity != null;
    }

    public List<BookEntity> getFavorites() {
        return bookDao.queryForAll();
    }

    public void addToFavorites(final BookEntity newBook) {
        bookDao.createOrUpdate(newBook);
    }

    public void removeFromFavorites(final String id) {
        bookDao.deleteById(id);
    }
}
