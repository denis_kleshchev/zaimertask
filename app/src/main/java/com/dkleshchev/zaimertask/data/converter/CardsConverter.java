package com.dkleshchev.zaimertask.data.converter;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.dkleshchev.zaimertask.data.network.model.BookItem;
import com.dkleshchev.zaimertask.data.network.model.ImageLinks;
import com.dkleshchev.zaimertask.data.network.model.VolumeInfo;
import com.dkleshchev.zaimertask.model.Card;

import java.util.ArrayList;
import java.util.List;

public class CardsConverter extends CollectionConverter<List<Card>, List<BookItem>> {
    @Override
    protected List<BookItem> fromBusinessNullSafe(@NonNull List<Card> business) {
        throw new UnsupportedOperationException("Can't convert Card to List<BookItem>");
    }

    @Override
    protected List<Card> fromDtoNullSafe(@NonNull List<BookItem> items) {
        final List<Card> result = new ArrayList<>();
        if (!items.isEmpty()) {
            for (BookItem item : items) {
                final VolumeInfo volumeInfo = item.getVolumeInfo();
                final List<String> authors = volumeInfo.getAuthors();
                final ImageLinks imageLinks = volumeInfo.getImageLinks();
                final String thumbnail = imageLinks != null ? imageLinks.getSmallThumbnail() : "";
                final Object[] authorsArray = authors != null ? authors.toArray() : new String[0];

                final Card card =
                        new Card(item.getId(),
                                volumeInfo.getTitle(),
                                TextUtils.join(", ", authorsArray),
                                thumbnail,
                                volumeInfo.getPreviewLink(),
                                false);
                result.add(card);
            }
        }
        return result;
    }
}
