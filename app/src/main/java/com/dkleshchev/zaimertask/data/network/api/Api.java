package com.dkleshchev.zaimertask.data.network.api;

import android.content.Context;

import com.dkleshchev.zaimertask.R;
import com.dkleshchev.zaimertask.utils.NetworkUtils;

import javax.inject.Inject;

import lombok.Getter;
import okhttp3.OkHttpClient;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {
    @Getter
    private final BooksApi booksApi;

    @Inject
    public Api(final Context context, final OkHttpClient okHttpClient) {
        final String endpoint = context.getString(R.string.url_books_endpoint);
        booksApi = NetworkUtils.createRestAdapter(BooksApi.class, okHttpClient, GsonConverterFactory.create(), endpoint);
    }
}
