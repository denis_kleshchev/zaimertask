package com.dkleshchev.zaimertask;

import android.app.Application;

import com.dkleshchev.zaimertask.di.AppModule;
import com.dkleshchev.zaimertask.di.DaggerComponent;
import com.dkleshchev.zaimertask.di.Injector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZaimerApplication extends Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(ZaimerApplication.class.getSimpleName());

    @Override
    public void onCreate() {
        super.onCreate();
        LOGGER.debug("onCreate()");

        Injector.setComponent(DaggerComponent.builder()
                .appModule(new AppModule(this))
                .build());
    }
}
