package com.dkleshchev.zaimertask.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func0;

public class SubscriptionManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionManager.class.getSimpleName());

    private final List<Subscription> subscriptions = new ArrayList<>();

    public <T> Subscription subscribe(final Observable<T> observable,
                                      final Subscriber<T> subscriber) {
        return processIfNotNull(observable, () -> observable.subscribe(subscriber));
    }

    public <T> Subscription subscribe(final Observable<T> observable,
                                      final Action1<T> next) {
        return processIfNotNull(observable, () -> observable.subscribe(next));
    }

    public <T> Subscription subscribe(final Observable<T> observable,
                                      final Action1<T> next,
                                      final Action1<Throwable> error) {
        return processIfNotNull(observable, () -> observable.subscribe(next, error));
    }

    public <T> Subscription subscribe(final Observable<T> observable,
                                      final Action1<T> next,
                                      final Action1<Throwable> error,
                                      final Action0 complete) {
        return processIfNotNull(observable, () -> observable.subscribe(next, error, complete));
    }

    public Subscription startManagingSubscription(final Subscription subscription) {
        LOGGER.debug("start managing subscription: {}", subscription);
        subscriptions.add(subscription);
        return subscription;
    }

    public final void stopManagingSubscription(final Subscription subscription) {
        if (subscriptions.contains(subscription) && !subscription.isUnsubscribed()) {
            LOGGER.debug("stop managing subscription: {}", subscription);
            subscription.unsubscribe();
        }
        subscriptions.remove(subscription);
    }

    public final void closeManagedSubscriptions() {
        for (final Subscription subscription : subscriptions) {
            if (!subscription.isUnsubscribed()) {
                LOGGER.debug("stop managing subscription: {}", subscription);
                subscription.unsubscribe();
            }
        }
        subscriptions.clear();
    }

    private <T> Subscription processIfNotNull(final Observable<T> observable,
                                              final Func0<Subscription> function) {
        if (observable != null) {
            return startManagingSubscription(function.call());
        }
        return null;
    }

}
