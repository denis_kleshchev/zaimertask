package com.dkleshchev.zaimertask.manager;

import android.content.Context;
import android.support.annotation.NonNull;

import com.dkleshchev.zaimertask.R;
import com.dkleshchev.zaimertask.data.converter.CardsConverter;
import com.dkleshchev.zaimertask.data.converter.EntityToCardConverter;
import com.dkleshchev.zaimertask.data.database.DatabaseHelper;
import com.dkleshchev.zaimertask.data.database.entity.BookEntity;
import com.dkleshchev.zaimertask.data.network.api.Api;
import com.dkleshchev.zaimertask.model.Card;
import com.dkleshchev.zaimertask.utils.RxUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

public class BookManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookManager.class.getSimpleName());

    private final Context context;
    private final Api api;
    private final DatabaseHelper databaseHelper;

    @Inject
    public BookManager(Context context, Api api, DatabaseHelper databaseHelper) {
        this.context = context;
        this.api = api;
        this.databaseHelper = databaseHelper;
    }

    public Observable<List<Card>> searchBooks(final String query) {
        return Observable.just(query)
                .flatMap(s -> api
                        .getBooksApi()
                        .getBooksList(s, context.getString(R.string.google_books_api_key))
                        .compose(RxUtils.applySchedulers()))
                .flatMap(RxUtils.transformAsync(kind -> new CardsConverter().fromDto(kind.getItems())))
                .flatMap(checkInFavorites())
                .compose(RxUtils.applySchedulers());
    }

    public Observable<List<Card>> getFavoriteBooks() {
        return RxUtils.runAsync(() ->
                databaseHelper.getZaimerTaskDao().getFavorites())
                .flatMap(RxUtils.transformAsync(bookEntities -> new EntityToCardConverter().fromBusiness(bookEntities)))
                .compose(RxUtils.applySchedulers());
    }

    public Observable<Boolean> swapFavorite(final Card card) {
        return RxUtils.runAsync(() -> {
            final boolean inFavorites = databaseHelper.getZaimerTaskDao().isInFavorites(card.getId());
            final BookEntity bookEntity = new EntityToCardConverter().fromDto(card);
            if (inFavorites) {
                databaseHelper.getZaimerTaskDao().removeFromFavorites(card.getId());
                return false;
            } else {
                databaseHelper.getZaimerTaskDao().addToFavorites(bookEntity);
                return true;
            }
        })
                .compose(RxUtils.applySchedulers());
    }

    @NonNull
    private Func1<List<Card>, Observable<List<Card>>> checkInFavorites() {
        return RxUtils.transformAsync(cards -> {
            for (Card card : cards) {
                final boolean inFavorites = databaseHelper.getZaimerTaskDao().isInFavorites(card.getId());
                LOGGER.debug("Book {} is in favorites: {}", card.getId(), inFavorites);
                card.setInFavorites(inFavorites);
            }
            return cards;
        });
    }
}
