package com.dkleshchev.zaimertask.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Card implements Parcelable, Serializable {
    private String id;
    private String title;
    private String author;
    private String coverLink;
    private String downloadLink;
    @Setter
    private boolean inFavorites;

    protected Card(Parcel in) {
        super();
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(coverLink);
        dest.writeString(downloadLink);
        dest.writeInt(inFavorites ? 1 : 0);
    }

    public void readFromParcel(Parcel in) {
        id = in.readString();
        title = in.readString();
        author = in.readString();
        coverLink = in.readString();
        downloadLink = in.readString();
        inFavorites = in.readInt() == 1;
    }

    public static final Creator<Card> CREATOR = new Creator<Card>() {
        @Override
        public Card createFromParcel(Parcel in) {
            return new Card(in);
        }

        @Override
        public Card[] newArray(int size) {
            return new Card[size];
        }
    };

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Card && TextUtils.equals(((Card) obj).getId(), this.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
